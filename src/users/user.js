import Mealplan from './mealplan.js';

export default class User {
	#sessionData
	#mpstore

	constructor({ mpstore }, session){
		this.#mpstore = mpstore;
		this.#sessionData = session;
	}

	get session() {
		return this.#sessionData;
	}

	get uid(){
		return this.session?.id;
	}

	get isAnonymous(){
		return !this.uid;
	}

	async getMealplan(){
		if(this.isAnonymous) {
			throw new Error('You must be authenticated to use the meal planner.');
		}
		return new Mealplan(
			data => this.#mpstore.set(this.uid, data),
			await this.#mpstore.get(this.uid)
		);
	}
}