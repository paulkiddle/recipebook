import User from './user.js';

export default function UserFactory({ mpstore }) {
	return session => new User({ mpstore }, session);
}