import Mealplan from './mealplan.js';

test('Mealplan', ()=>{
	const update = ()=>{};
	const mp = new Mealplan(
		update,
		{
			recipes: [{name:'a'}],
			plan: [{day:'Mon', name:'b'}]
		}
	);

	expect(mp.recipes).toMatchSnapshot();
	expect(mp.plan).toMatchSnapshot();
})