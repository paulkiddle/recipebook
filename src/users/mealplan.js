export default class Mealplan {
	#data
	#update;

	constructor(update, { recipes, plan }){
		this.#update = update;
		if(!recipes || !plan) {
			throw new TypeError('Recipes and plan must both be specified');
		}
		this.#data = { recipes, plan };
	}

	get recipes(){
		return this.#data.recipes;
	}

	get plan(){
		return this.#data.plan;
	}

	async update({ recipes, plan }){
		await this.#update({ recipes, plan });
		this.#data = { recipes, plan };
	}
}
