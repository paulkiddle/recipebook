import path from 'path';
import fs from 'fs';

export class AssetStore {
	#blobStore
	#map = new Map();

	constructor(store) {
		this.#blobStore = store;
	}

	async register(asset, ext, data){
		const writer = this.#blobStore.createWriteStream({ext});

		for await(const chunk of data) {
			writer.write(chunk);
		}

		const key = await new Promise((resolve) => {
			writer.end(
				()=>resolve(writer.key)
			);
		});

		this.#map.set(asset, [key, ext].join('.'));
	}
	getId(asset){
		const a = this.#map.get(asset);
		if(!a) {
			console.warn('Asset not registered', asset);
		}
		return a;
	}
	getData(file) {
		const [key, ext] = file.split('.');
		return this.#blobStore.createReadStream({key, ext});
	}
}

export async function assetMapper(assetStore, assets=[], prefix = '/assets/') {
	for(const asset of assets) {
		await assetStore.register(asset, asset.ext, asset.file);
	}

	const mapper = value => {
		if(value && value.file) {
			return prefix + assetStore.getId(value);
		}
		return value;
	};

	mapper.serve = (req, res) => {
		const { pathname } = new URL('file://' + req.url);
		if(!pathname.startsWith(prefix)) {
			return false;
		}

		const key = pathname.substr(prefix.length);

		assetStore.getData(key).pipe(res);
		return true;
	};

	return mapper;
}

export function asset(src, source = 'file:///') {
	const file = new URL(src, source);

	return {
		ext: path.extname(src).substr(1),
		file: fs.createReadStream(file)
	};
}

export function assetInline(source, ext) {
	return {
		ext,
		file: [source]
	};
}
