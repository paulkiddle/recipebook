export default class Request {
	#req

	static url(request){
		let path = request.path;

		if(request.method === 'OPTIONS') {
			path += '!';
		}

		return path;
	}

	constructor(req) {
		const { pathname, searchParams } = new URL(req.url, 'file://');

		this.path = pathname;
		this.query = searchParams;
		this.method = req.method;
		this.#req = req;

		if(this.method === 'GET' && this.path.endsWith('!')) {
			this.method = 'OPTIONS';
			this.path = this.path.slice(0, -1);
		}
	}

	async * [Symbol.asyncIterator](){
		yield* this.#req;
	}

	async getBody(){
		let body = '';
		for await(const chunk of this) {
			body+=chunk;
		}
		return body;
	}

	async getData(){
		return new URLSearchParams(await this.getBody());
	}

	toString(props={}){
		return Request.url({ ...this, ...props });
	}

	redirect(props){
		return res => {
			res.statusCode = 303;
			const location = typeof props === 'string' ? props : this.toString(props);
			res.setHeader('Location', location);
		};
	}
}
