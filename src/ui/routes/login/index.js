import view from './view.js';

export default allowedDomains => async (req, auth) => {
	if(allowedDomains.length === 1) {
		return req.redirect(await auth.getAuthUrl(allowedDomains[0]));
	}

	if(req.method === 'POST') {
		const url = await view.parse(req);
		return req.redirect(await auth.getAuthUrl(url));
	}

	return view();
};
