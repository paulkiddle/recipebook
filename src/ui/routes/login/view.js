import html from 'encode-html-template-tag';
import form from '../form.js';

export default form(
	form => () => form({}, html`<label>OAuth URL<input type="url" name="url"></label>`),
	body => new URL(body.get('url'))
);
