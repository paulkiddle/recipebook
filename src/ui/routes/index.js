import html from 'encode-html-template-tag';
import jwt from 'jsonwebtoken';
import es from 'expressive-switch';
import addRecipeForm from './addRecipe/index.js';
import listRecipes from './listRecipes/index.js';
import viewRecipePage from './viewRecipe/index.js';
import Login from './login/index.js';
import MealplanForm from './mealplan/mealplan.js';

const pleaseLogin = html`Please log in.`;

const matchRoute = (subject, route) => {
	if(route instanceof RegExp) {
		return subject.match(route);
	} else {
		return subject === route ? [subject] : null;
	}
};

export default (services, options, auth) => {
	const addRecipe = addRecipeForm(services);

	function canDeleteRecipe(session, recipe){
		if(!session){
			return false;
		}

		if(recipe.author === session.id) {
			return true;
		}

		return session.id === options.superUser;
	}

	async function deleteRecipe(slug, session) {
		const r = await services.get(slug);
		if(canDeleteRecipe(session, r)) {
			const id = r.id;
			await services.delete(id);
			return { redirect: '/' };
		} else {
			throw new Error('Forbidden');
		}
	}

	const list = listRecipes({
		list: ()=> services.list(),
		canDeleteRecipe
	});
	const viewRecipe = viewRecipePage({
		canDeleteRecipe
	});

	const login = Login(options.authDomains.map(d => 'https://'+d));

	return async (req, user) => {
		const session = user.session;
		const key = options.csrfKey;

		const csrf = {
			getToken: ()=>jwt.sign({
				id: session.id
			}, key),
			verifyToken: token=>jwt.verify(token, key).id === session.id
		};

		const [s,c] = es();
		let params;
		let recipe;

		switch(s(route => params = matchRoute(req.path, route))) {
		case c('/'):
		case c('/recipes'):
			return list();
		case c('/login'):
			return login(req, auth);
		case c('/add'):
			return session ?
				addRecipe(req, session, csrf) : pleaseLogin;
		case c('/delete'):
			return session && csrf.verifyToken(req.query.get('csrf')) ?
				deleteRecipe(req.query.get('id'), session) : pleaseLogin;
		case c('/mealplan'):
			return MealplanForm(
				await user.getMealplan(),
				req.method==='POST' && await req.getData(),
				req.method
			);
		case c(/^\/recipes\/([^/]+)/i):
			recipe = await services.get(params[1]);
			if(!recipe) { break; }
			return viewRecipe(recipe, session, session && csrf.getToken());
		}

		return res => {
			res.statusCode = 404;
			return html`<div>Page not found</div>`;
		};
	};
};

export const assets = [];
export const urls = {
	index: '/'
};
