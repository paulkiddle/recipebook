import html from 'encode-html-template-tag';

export default async (/*error*/) => {
	return res => {
		res.statusCode = 500;
		return html`<div>Internal server error</div>`;
	};
};
