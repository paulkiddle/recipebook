import html from 'encode-html-template-tag';
import user from '../../templates/user.js';

export default recipes => recipes.map(recipe => html`
	<div>
		<h2><a href="/recipes/${recipe.slug}">${recipe.title}</a></h2>
		<p>By ${user({ name: recipe.name, id: recipe.uri })}</p>

		<p>${recipe.description}</p>
	</div>
`);
