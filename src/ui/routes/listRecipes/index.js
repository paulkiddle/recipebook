import listRecipes from './view.js';

// Options:
//superUser
//list
export default (options) => {
	return async () => listRecipes(
		await options.list()
	);
};
