import postForm from '../form.js';
import html from 'encode-html-template-tag';

const style = html`<style>
.RecipeForm {
	width: 100%;
	margin: 0 -5px;
}

.RecipeForm label,
.RecipeForm label > * {
	display: block;
}

.RecipeForm label {
	font-weight: bold;
	margin: 5px;
	flex-grow: 1;
	flex-basis: 100%;
	margin: 15px 5px;
}

.RecipeForm button {
	margin: 15px 5px;
}

.RecipeForm textarea {
	min-height: 200px;
	width: 100%;
}

.RecipeForm__content {
	display: flex;
}
</style>`;


const component = html`
${style}
<label>Title<input name="title"></label>
<label>Serves<input name="serves" type="number" value="1"></label>
<label>Description<textarea name="description"></textarea></label>
<div class="RecipeForm__content">
	<label>Ingredients (one ingredient per line)<textarea name="ingredients"></textarea></label>
	<label>Method<textarea name="method"></textarea></label>
</div>
<button>Submit</button>
`;

const addRecipeForm = withCsrf => withCsrf(postForm)(
	form => () => html`
		<h2>Add Recipe</h2>
		${form({ class: 'RecipeForm' }, component)}
	`);

addRecipeForm.parse =
	body => ({
		title: body.get('title'),
		serves: parseInt(body.get('serves'), 10),
		description: body.get('description'),
		ingredients: body.get('ingredients'),
		method: body.get('method')
	});

export default addRecipeForm;
