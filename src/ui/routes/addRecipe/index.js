import html from 'encode-html-template-tag';
import recipeForm from './addRecipe.js';

const getCsrfWrapper = (getToken, verifyToken) => formFn => (getComponent, parse) => formFn(
	formWrapper => getComponent((attrs, children) => formWrapper(attrs, [
		html`<input name="csrf" type="hidden" value="${getToken()}">`,
		children
	])),
	requestData => {
		const token = requestData.get('csrf');
		if(!verifyToken(token)) {
			throw new Error('CSRF failed');
		}
		return parse(requestData);
	}
);

export default db => async (req, session, csrf) => {
	const withCsrf = getCsrfWrapper(
		csrf.getToken,
		csrf.verifyToken
	);

	const form = recipeForm(withCsrf);

	if(req.method === 'POST') {
		const data = await recipeForm.parse(await req.getData());

		const slug = data.title.replace(/[^a-z0-9]+/ig, '-').toLowerCase();
		const created = new Date().toISOString();

		await db.add({ author: session, slug, created, ...data });

		return req.redirect('/');
	}

	return form();
};
