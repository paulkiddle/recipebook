import html from 'encode-html-template-tag';
import user from '../../templates/user.js';

function controls(recipe, token){
	return html`<a href="/delete?id=${recipe.slug}&csrf=${token}">Delete</a>`;
}

export default (recipe, canEdit, token) => html`
	<div>
		<h2>${recipe.title}</h2>
		<p>By ${user({ name: recipe.name, id: recipe.uri })}</p>

		<p>Serves ${recipe.serves}</p>

		${recipe.description.split('\r\n').map(d =>
		html`<p>${d}</p>`)}

		<h3>Ingredients</h3>
		<ul>
			${recipe.ingredients.split('\r\n').map(i => html`<li>${i}</li>`)}
		</ul>

		<h3>Method</h3>
		${recipe.method.split('\r\n').map(m =>
		html`<p>${m}</p>`)}

		<div>${canEdit ? controls(recipe, token) : ''}</div>
	</div>
`;
