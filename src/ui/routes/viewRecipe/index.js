import view from './view.js';

export default (options) => {
	return async (recipe, session, token) => view(
		recipe,
		options.canDeleteRecipe(session, recipe),
		token
	);
};
