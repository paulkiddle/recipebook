import html from 'encode-html-template-tag';

export function MealForm(mealplan) { return html`<form method="POST">
	<style>
	form {
		display:grid;
		min-height:100%;
		grid-template-rows: 1fr min-content;
		grid-template-columns: 1fr 1fr;
	}
	label { display: flex; flex-direction: column; }
	textarea { flex: 1; }
	fieldset { grid-column: span 2; }

	</style>
	<label>
		Recipes
		<textarea name="recipes">${mealplan.recipes.map(
		r=>html`${r.name} ${r.uri??''}\n`
	)}</textarea>
	</label>
	<label>
		Plan
		<textarea name="plan">${mealplan.plan.map(
		r=>html`${r.day} ${r.name} ${r.uri??''}\n`
	)}</textarea>
	</label>
	<fieldset>
		<button>Save</button>
	</fieldset>
	</form>`;
}

MealForm.parse = formData => ({
	recipes: formData.get('recipes').trim().split('\n').map(row => row.match(/^(?<name>.+?)\s*(?<uri>https?:\/\/.+)?$/).groups),
	plan: formData.get('plan').trim().split('\n').map(row => row.match(/^(?<day>\S+)\s*(?<name>.+?)?\s*(?<uri>https?:\/\/.+)?$/).groups)
});

export default async (mealplan, formData, method) => {
	if(method === 'POST'){
		await mealplan.update(MealForm.parse(formData));
	}
	
	return MealForm(mealplan);
};