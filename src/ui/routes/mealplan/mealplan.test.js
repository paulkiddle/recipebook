import MealplanForm, { MealForm } from './mealplan.js';

test('Mealplan form', async ()=>{
	expect(await (await MealplanForm({
		recipes: [{ name: 'a', uri:'http://www'}],
		plan: [{ day:'Mon', uri:'http://www', name: 'b'}]
	})).render()).toMatchSnapshot();
});

test('Form parse', () => {
	expect(MealForm.parse(new URLSearchParams('recipes=a\nb&plan=b d\nc n'))).toMatchSnapshot()
})