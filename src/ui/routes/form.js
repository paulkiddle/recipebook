import html from 'encode-html-template-tag';

const getBody = req => new Promise((resolve, reject) => {
	let body = '';
	req.on('data', chunk => body += chunk);
	req.on('end', () => resolve(body));
	req.on('error', reject);
});

const attrs = params => params ? Object.entries(params).map(([name, value])=>html` ${name}="${value}"`) : '';
const element = name => (params, children) => html`<${name}${attrs(params)}>${children}</${name}>`;

const form = (getComponent, parse) => {
	const form = element('form');
	const component = getComponent(form);
	component.parse = parse;
	return component;
};

const postForm = (getComponent, parse) => form(
	form => {
		const postForm = (params, children) => form({ method: 'POST', ...params }, children);
		return getComponent(postForm);
	},
	async req => parse(new URLSearchParams(await getBody(req)))
);

export default postForm;
