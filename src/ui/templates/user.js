import html from 'encode-html-template-tag';

const user = ({ name, id }) => html`<a href="${id}">${name}</a>`;

export default user;
