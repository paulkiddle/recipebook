import html from 'encode-html-template-tag';
import {wrapper as page} from 'sp-templates';
import userCard from './user.js';

const style = html`
<style>
html, body {
	margin: 0;
	padding: 0;
	height: 100%;
}
* {
	box-sizing: border-box;
}
body {
	display: flex;
	flex-direction: column;
	font-family: Arial, sans-serif;
}

main.content {
	flex-grow: 1;
	margin-top: 30px;
}
main h1, main h2 {
	color: var(--theme-colour);
}
header {
	background: var(--theme-colour);
	color: white;
	min-height: 170px;
	display: flex;
}
header a:link,
header a:visited {
	color: white;
}
.content {
	width: 100%;
	max-width: 1200px;
	margin: 0 auto;
	padding: 10px;
}
.siteInfo{
	display: flex;
	justify-content: space-between;
}
.login{
	display: inline-block;
	background: #262626;
	padding: 3px 10.5px 4px;
	border-radius: 8px;
	border-top-left-radius: 0;
	border-top-right-radius: 0;
	margin-top: -10px;
	font-size: 11px;
	letter-spacing: 0.5px;
}
footer {
	background: #F1F1F1;
	color: var(--theme-color);
}
header h1 {
	font-weight: normal;
	margin: 32px 0 0 0;
}
main h1 {
	font-weight: normal;
	letter-spacing: -0.05em;
	margin-top: 0;
}
header .content {
	display: flex;
	flex-direction: column;
	justify-content: space-between;
}
nav {
	margin-bottom: -10px;
}
nav a {
	display: inline-block;
	background: rgba(0,0,0,0.3);
	border-radius: 8px;
	border-bottom-left-radius: 0;
	border-bottom-right-radius: 0;
	padding: 5px 10.5px 5px;
	letter-spacing: 0.5px;
}
.User a {
	display: block;
	border-radius: 10px;
	padding: 5px;
	background: rgba(0,0,0,0.3);
	min-width: 100px;
	text-align: center;
}
</style>
`;

export default ({title, colour, authUrl, user}, body) => page(
	{title, style:`--theme-colour: ${colour || '#99cc00'}`},
	html`
	${style}
	<header>
		<div class="content">
			<div class="siteInfo">
		  	<h1>${title}</h1>
				<div>
					${user ? html`<span class="User">${userCard(user)}</span>` : html`<a href="${authUrl}" class="login">Log in</a>`}
				</div>
			</div>
			<nav>
				<a href="/">Home</a>
				${user ? html`<a href="/add">Add new</a>` : ''}
			</nav>
		</div>
	</header>
	<main class="content">
		${body}
	</main>
	<footer>
		<div class="content">`
);
