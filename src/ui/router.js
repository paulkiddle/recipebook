import wrapper from './templates/sp.js';
import Routes, { assets } from './routes/index.js';
import errorRoute from './routes/error.js';
import Request from './request.js';

export const routeAssets = assets;

export default (services, options, auth) => {
	const { title, renderer } = options;
	const routes = Routes(services, options, auth);

	async function route(req, user){
		try {
			return await routes(req, user);
		} catch(e) {
			console.warn(e);
			return errorRoute(e);
		}
	}

	return async (req, res) => {
		const user = await auth.getUser(req);
		const request = new Request(req);
		const response = await route(request, user);
		const body = typeof response === 'function' ? response(res) : response;
		if(body) {
			const output = await wrapper(
				{
					title,
					user: user.session,
					authUrl: '/login'
				}, body).render(renderer);
			res.end(output);
		} else {
			res.end();
		}
	};
};
