import { AssetStore, assetMapper } from './assets.js';
import Router, { routeAssets } from './router.js';

export default async ({ title, assets, ...options }, services, auth) => {
	const assetStore = new AssetStore(assets);
	const mapper = await assetMapper(assetStore, routeAssets);
	const router = Router(services, {
		superUser: options.superUser,
		authDomains: options.authDomains,
		csrfKey: options.csrfKey,
		title, renderer: mapper
	}, auth);

	return async function appRouter(req, res) {
		try {
			if(mapper.serve(req, res)) {
				return;
			}

			await router(req, res);
		} catch(e) {
			console.warn(e);
			res.statusCode = 500;
			res.end('Internal server error.');
		}

		return;
	};
};
