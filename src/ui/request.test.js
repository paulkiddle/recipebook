import Request from './request.js';
import stream from 'stream';
import { jest } from '@jest/globals'

test('Request to url', ()=>{
	expect(Request.url({ path: '/home' })).toEqual('/home');
	expect(Request.url({ path: '/home', method: 'OPTIONS' })).toEqual('/home!');
});

test('Stream to search params', async ()=>{
	const req = stream.Readable.from(['a=b&c=d']);
	req.url = '/';

	const r = new Request(req);

	const p = await r.getData();
	expect(p).toBeInstanceOf(URLSearchParams);
	expect(p.toString()).toBe('a=b&c=d')
});

test('Redirect fn', ()=>{
	const req = {
		method: 'GET',
		url: '/!'
	};

	const r = new Request(req).redirect({ method: 'GET' });

	const res = {
		setHeader: jest.fn()
	}

	r(res);

	expect(res.statusCode).toBe(303);
	expect(res.setHeader).toHaveBeenCalledWith('Location', '/');
})
