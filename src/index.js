import ui from './ui/index.js';
import services from './services/index.js';
import Auth, { migrate } from './auth.js';
import UserFactory from './users/index.js';
import MealplanStore from './services/meals.js';

const app = async (options = {}) => {
	const { title, origin, csrfKey, authDomains, superUser, sql } = options;
	const s = await services({ sql, origin });
	const redirectPath = '/authenticate';
	await migrate(sql);

	const mpstore = await MealplanStore.create(sql);

	const userFactory = new UserFactory({
		mpstore
	});

	const auth = new Auth(sql, {
		clientName: title || 'Recipebook',
		origin: origin,
		authPath: redirectPath,
		secret: csrfKey,
		domains: authDomains,
		userFactory
	});
	const u = await ui(
		{
			title: 'RecipeBook',
			superUser: superUser,
			authDomains: authDomains,
			csrfKey: csrfKey
		},
		s,
		auth
	);

	return async (req, res) => {
		try {
			if(await auth.loginUser(req, res)) {
				res.statusCode = 303;
				res.setHeader('location', '/');
				res.end();
				return;
			}

			return u(req, res);
		} catch(e) {
			console.warn(e);
			res.statusCode = 500;
			res.end(String(e));
		}
	};
};

export default app;
