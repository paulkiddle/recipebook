export default async function migrate(knex) {
	if(!await knex.schema.hasTable('users')) {
		await knex.schema.createTable('users', t=> {
			t.increments('id').primary();
			t.text('uri').unique();
			t.string('name');
			t.text('avatar');
		});
	}

	if(!await knex.schema.hasTable('recipes')) {
		await knex.schema.createTable('recipes', t=>{
			t.increments('id').primary();
			t.integer('author_id');
			t.foreign('author_id').references('users.id');
			t.string('title');
			t.timestamp('created').defaultTo(knex.fn.now());
			t.string('slug').unique();
			t.text('description');
			t.string('serves');
			t.text('ingredients');
			t.text('method');
		});
	}
}
