import Store from './index.js';
import Sqlite from 'knex-sqlite';

test('store add/get', async()=>{
	const sql = Sqlite(':memory:');
	const store = await Store({ sql, origin: 'http://example.com' });

	await store.add({
		author: {
			id: 'http://example.example/user',
			name: 'Paul'
		},
		title: 'title',
		slug:'slug',
		description: 'desc',
		serves: 2,
		ingredients: 'ingred',
		method: 'method'
	});

	const record = await store.get('slug');
	delete record.created;

	expect(record).toMatchSnapshot();
});
