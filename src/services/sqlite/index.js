import migrate from './migrate.js';

const store = async (options = {}) => {
	const { sql, origin } = options;

	await migrate(sql);

	return {
		origin,
		async list(){
			return await sql('recipes').select('*').leftJoin('users', 'recipes.author_id', 'users.id');
		},
		async get(slug){
			const [r] = await sql('recipes').select('*').leftJoin('users', 'recipes.author_id', 'users.id').where({slug});
			return r;
		},
		async add(data){
			const authorUri = data.author.id;
			await this.addUser(data.author);
			const [author] = await sql('users').select('id').where({ uri: authorUri });
			const authorId = author.id;

			await sql('recipes').insert({
				author_id: authorId,
				title: data.title,
				slug: data.slug,
				description: data.description,
				serves: data.serves,
				ingredients: data.ingredients,
				method: data.method
			});
		},
		async delete(id) {
			await sql('recipes').where({id}).delete();
		},
		async addUser(data) {
			await sql('users').insert({
				uri: data.id,
				name: data.name
			}).onConflict('uri')
				.merge({ name: data.name });
		}
	};
};

export default store;
