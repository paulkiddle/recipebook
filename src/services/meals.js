const tbls = (prefix='mealplan_') => ({
	plans: prefix + 'plans',
	recipes: prefix + 'recipes'
});

export default class Mealplan {
	static async create(knex, prefix) {
		const tables = tbls(prefix);
		
		if(!await knex.schema.hasTable(tables.recipes)) {
			await knex.schema.createTable(tables.recipes, t => {
				t.increments('id').primary();
				t.text('uid');
				t.text('uri').unique();
				t.text('name');
			});
		}

		if(!await knex.schema.hasTable(tables.plans)) {
			await knex.schema.createTable(tables.plans, t => {
				t.increments('id').primary();
				t.text('uid');
				t.text('day');
				t.text('name');
				t.text('uri');
			});
		}

		return new Mealplan(knex, prefix);
	}

	#recipes
	#plans

	constructor(knex, prefix){
		const tables = tbls(prefix);
		this.#recipes = () => knex(tables.recipes);
		this.#plans = () => knex(tables.plans);
	}

	async getRecipes(uid) {
		return await this.#recipes().select('uri', 'name').where({ uid });
	}

	async setRecipes(uid, recipes) {
		const records = recipes.map(({ name, uri })=>({
			name,
			uri,
			uid
		}));
		await this.#recipes().delete().where({ uid });
		return await this.#recipes().insert(records);
	}

	async getPlans(uid) {
		return await this.#plans().select('day', 'name', 'uri').where({ uid });
	}

	async setPlans(uid, plans){
		const records = plans.map(({ day, name, uri })=>({
			day,
			name,
			uri,
			uid
		}));
		await this.#plans().delete().where({ uid });
		return await this.#plans().insert(records);
	}

	async get(uid) {
		return {
			recipes: await this.getRecipes(uid),
			plan: await this.getPlans(uid)
		};
	}

	async set(uid, { recipes, plan }) {
		await this.setRecipes(uid, recipes);
		await this.setPlans(uid, plan);
	}

	// async export(){
	// 	const recipes = await this.#recipes().select('uid', 'uri', 'name');
	// 	const records = recipes.reduce((o, [uid, uri, name])=>{
	// 		const userRecord = o[uid] ??= { recipes:[], plans: [] };
	// 		userRecord.recipes.push({ uri, name });
	// 		return o;
	// 	}, {});

	// 	const plans = await this.#plans().select('uid', 'day', 'uri', 'name');

	// 	return plans.reduce((o, [uid, day, uri, name])=>{
	// 		const userRecord = o[uid] ??= { recipes:[], plans: [] };
	// 		userRecord.plans.push({ day, uri, name });
	// 		return o;
	// 	}, records);
	// }

	// async import(data){
	// }
}