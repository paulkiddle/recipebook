import KnexMastoAuth, {migrate} from 'knex-masto-auth';
import JwtCookie from 'jwt-cookie';

export { migrate };

export default class Auth {
	#auth
	#jwtc
	#authPath
	#domains
	#userfactory

	constructor(knex, { clientName, secret, origin, authPath, domains, userFactory }){
		const clientOptions = {
			clientName,
			redirectUri: origin+authPath
		};
		this.#authPath = authPath;
		this.#domains = domains;
		this.#auth = new KnexMastoAuth(knex, clientOptions, {});
		this.#jwtc = new JwtCookie(secret);
		this.#userfactory = userFactory;
	}

	getAuthUrl(instanceUrl) {
		return this.#auth.getRedirectUrl(instanceUrl);
	}

	async loginUser(req, res) {
		const { pathname } = new URL('file://' + req.url);
		if(pathname !== this.#authPath) {
			return;
		}

		const user = await this.#auth.getUserFromCallback(req);

		const { host } = new URL(user.sub);
		if(!this.#domains.includes(host)) {
			throw new Error(`Users from ${host} are not permitted to authenticate.`);
		}

		this.#jwtc.set(res, {
			name: user.name,
			id: user.sub
		});
		return true;
	}

	getUser(req) {
		return this.#userfactory(this.#jwtc.get(req));
	}
}
