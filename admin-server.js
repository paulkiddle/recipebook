import { RpcWrapper } from 'json-rpc-wrapper';

export default function (methods) {
	const proxy = new Proxy(methods, {
		get(target, string) {
			console.log(target, string);
			if(string in target) {
				return (...args) => {
					console.log('Called', target, string, ...args, target[string](...args))
					return target[string](...args);
				}
			}
		}
	})

	const rpc = new RpcWrapper(proxy);

	return async (req, res) => {
		console.log('r');
		let data = '';
		for await (const d of req) {
			data+=d;
		}
		console.log(data);

		const rpcRes = await rpc.callReq(data);
console.log(rpcRes);
		res.setHeader('Content-Type', 'application/json')
    res.write(JSON.stringify(rpcRes))
    res.end()
	}
}
