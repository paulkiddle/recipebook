# Create empty file
echo "" > docs/api.md
# Generate docs and trim trailing whitespace and useless stuff
./node_modules/.bin/jsdoc2md src/*.js | sed 's/[ \t]*$//;s/\*\*Kind\*\*: global [^\n]*//' >> docs/api.md

echo "\n## Dependencies" > docs/dependencies.md
node -e "console.log(Object.entries(require('./package.json').dependencies).map(e=>' - '+e.join(': ')).join('\n'))" >> docs/dependencies.md

cat docs/readme.md docs/dependencies.md docs/api.md > README.md
