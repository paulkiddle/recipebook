import start from 'simple-wsgi';
import rpc from './admin-server.js';
import main, {admin} from './index.js';
import blobStore from 'content-addressable-blob-store';

const port = process.env.PORT || 8080;
const adminPort = process.env.ADMIN_PORT || (parseInt(port,10)+1);
const origin = process.env.ORIGIN || 'http://localhost:' + port;

const app = await main({
	origin,
	assets: blobStore('./assets')
});

start(app, port);
start(rpc(admin), adminPort);
