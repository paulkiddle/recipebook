
## Dependencies
 - encode-html-template-tag: ^2.2.1
 - expressive-switch: ^1.0.0
 - jsonwebtoken: ^8.5.1
 - jwt-cookie: ^1.0.0
 - knex-masto-auth: ^1.0.0
 - knex-sqlite: ^1.0.0
 - simple-wsgi: ^1.1.0
 - sp-templates: ^1.3.1
