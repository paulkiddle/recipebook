import start from 'simple-wsgi';
import main from './src/index.js';
import connect from 'knex-sqlite';

const port = process.env.PORT || 8080;
const origin = process.env.ORIGIN || 'http://localhost:' + port;

main({
	sql: connect('data.db', undefined, {
		useNullAsDefault: true
	}),
	superUser: 'https://kith.kitchen/users/Paul',
	authDomains: [
		'kith.kitchen'
	],
	origin,
	csrfKey: process.env.CSRF_KEY || 'none'
}).then(app => start(app, port));
