import lib from '../src/index.js';

test('Example test', async ()=>{
	expect(await lib({
		assets: {
			createWriteStream(){
				return {
					write(){},
					end(cb){cb(null, {});}
				};
			}
		},
		sql: {
			schema: {
				hasTable(){
					return true;
				}
			}
		},
		authDomains: []
	})).toBeTruthy();
});
